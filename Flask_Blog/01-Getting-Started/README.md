### create the project directory 
    mkdir FLASK_APP

### go to te FLASK_APP directory 
    cd FLASK_APP
    
### create a virtual environment 
    virtualenv -p python3 evn 
    
### activate the virtual environment 
    sourc env/bin/activate 
    
### install the FLASK 
    pip install flask 
    
### create a directory Getting-Started with and switch to that directory 
    mkdir 01-Getting-Started
    cd 01-Getting-Started 
    
### create a file flaskblog.py  
    touch flaskblog.py 
    
### open the file flaskblog.py and enter the below 
    1. from flask import Flask
    2. app = Flask(__name__)
    
    3. @app.route("/")    
    4. def home():
    5.     return "Home Page"
    
- line 1. importing the flask module 
- line 2. creating the app variable, __name__ contains the current file
- line 3. route '/' to the view method "home"
- line 4-5. view logic, just returning the string "home page" 

### close the file and set the below environment variable and run the project 
    $ export FLASK_APP=flaskblog.py 
    $ export FLASK_DEBUG=1 
    $ flask run 
    
- FLASK_DEBUG=1, take care the change made in the view logic, so that we do 
not press ctrl+C and run "flask run" again and again 

### open the browser and fire the url below 
    http://127.0.0.1:5000/  
    or 
    http://localhost:5000/
    
### open the file flaskblog.py and enter the below  end edit the below 
    1. from flask import Flask
    2. app = Flask(__name__)
    
    3. @app.route("/")    
    4. def home():
    5.     return "<h1>Home Page</h1>"
    
    6. @app.route("/about")
    7. def about():
    8.     return "<h1>About Page</h1>"
    
### open the browser and fire the url below 
    http://127.0.0.1:5000/  
    http://127.0.0.1:5000/about
    
    
### you can apply two decorators like below in the home view 

    from flask import Flask
    app = Flask(__name__)
    
    
    @app.route("/") 
    @app.route("/home") # <----
    def home():
        return "<h1>Home Page</h1>"
    
    
    @app.route("/about")
    def about():
        return "<h1>About Page</h1>"
        
### open the browser and fire the url below 
    http://127.0.0.1:5000/  
    http://127.0.0.1:5000/home
    http://127.0.0.1:5000/about
    
    
### to  run app directly form the python like below 
    python flaskblog.py 
    
 
- edit the file flaskblog.py and add the code below 


    from flask import Flask
    app = Flask(__name__)    
     
    ------
    
    if __name__ == '__main__': # <---
        app.run(debug=True)    # <---
              
- the added line calls when the command python flaskblog.py is directly run
 from the command line. and its simple means that run the app in the debug 
 mode i.e replacement of the export FLASK_DEBUG=1 and flask run 

        


        


