## This tutorial is on the integrating template in the flask app 
 
### create the folder templates at the same level as shown below 
    ├── flaskblog.py
    ├── static
    └── templates
    
### create the home.html  in the templates folder 
    $ touch home.html 
    
### paste the basic html content in the home.html like below 
    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
    </head>
    <body>
    
    {% for post in posts %}
    <h1>{{ post.title }}</h1>
    <p>By- {{post.author}} on date: {{ post.date_posted }}
    <p>{{ post.content }}</p>
    
    {% endfor %}
    
    </body>
    </html>
    
### go to te flaskblog.py and edit the code for home method like below 

    from flask import Flask, render_template
    app = Flask(__name__)
    
    posts = [
        {
            'author': 'Corey Schafer',
            'title': 'Blog Post 1',
            'content': 'First post content',
            'date_posted': 'April 20, 2018'
        },
        {
            'author': 'Jane Doe',
            'title': 'Blog Post 2',
            'content': 'Second post content',
            'date_posted': 'April 21, 2018'
        }
    ]
    
    @app.route("/")
    @app.route("/home")
    def home():
        return render_template('home.html', posts=posts)    
     
     
- render_template(), is use to render contents of html like home.html 
- dynamic variable can be passed to the html using render_template like "posts"

 
### similarly create the about.html file in the folder templates and paste the content below  
    
    <!DOCTYPE html>
    <html>
    <head>
        {% if title %}
            <title>Flask App - {{title}}</title>
        {% else %}
            <title>Flask App </title>
        {% endif %}
    </head>
    <body>
        
        <p>About Page </p>
        
    </body>
    </html>
    
    
### edit the file flaskblog.py and edit the about view like below 

    @app.route("/about")
    def about():
        return render_template('about.html', title='About')
        
        
### separate the similar content from the about.html and home.html in the different file layout.html 

    <!DOCTYPE html>
    <html>
    <head>
        {% if title %}
        <title>Flask App - {{title}}</title>
        {% else %}
        <title>Flask App </title>
        {% endif %}
    </head>
    <body>
    
    {% block content %}  {% endblock %} 
    
    </body>
    </html>

        
### edit the content of home.html like below 
    {% extends "layout.html" %}
    {% block content %}
        {% for post in posts %}
            <h1>{{ post.title }}</h1>
            <p>By- {{post.author}} on date: {{ post.date_posted }}
            <p>{{ post.content }}</p>
        {% endfor %}
    {% endblock content %}
    
    
### edit the content of about.html like below 
    {% extends "layout.html" %}
    {% block content %}
           <p>About Page </p>
    {% endblock content %}
    
### open the browser and look the url 
    http://127.0.0.0:5000/home
    http://127.0.0.0:5000/about
    
### how static url called in the template 
- make a static directory 
- save your static file in the static directory 
- import url_for in the view file like flaskblog.py 
- use url_for in the html like example below 

###
    <link rel="stylesheet" type="text/css" href="{{ url_for('static', filename='main.css') }}">

    
    
    

    
    
    
    
    
        
    